﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;


namespace IkeaApi.Controllers
{
    [ApiController]
    [Authorize]
    [Route("[controller]")]
    public class IkeaNamesController : ControllerBase
    {

        private string[] Syllables = {"pot", "mi", "ham", "nus", "vek", "hal", "ruk", "tor" };
        private string[] Endings = { "put", "sta", "vist", "mol", "vig" };

        

        public IkeaNamesController()
        {
         
        }

        /// <summary>
        /// Generates new IKEA product name
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public string Get()
        {
            var rnd = new Random();

            string newName = "";
            int syllablesCount = rnd.Next(1, 4);
            while (syllablesCount > 0)
            {
                newName += Syllables[rnd.Next(0, Syllables.Length)];
                syllablesCount--;
            }

            newName += Endings[rnd.Next(0, Endings.Length)];
            
            return newName;
        }
    }
}
