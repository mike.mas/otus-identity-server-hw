﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using IdentityModel.Client;


namespace IkeaApiClient
{
    class Program
    {

        private static string IkeaApiEndpoint = "https://localhost:44312";
        private static string IdentityServerEndpoint = "https://localhost:5001";


        static async Task Main(string[] args)
        {


            var token = await GetToken();
            Console.WriteLine("TOKEN");
            Console.WriteLine(token.Json);
            Console.WriteLine("TOKEN CLAIMS");
            Console.WriteLine(await MakeTokenClaimsRequest(token.AccessToken));
            Console.WriteLine("NAME");
            Console.WriteLine(await MakeNamesRequest(token.AccessToken));

            Console.ReadKey();
        }



        static async Task<TokenResponse> GetToken()
        {
            using (var client = new HttpClient())
            {
                var disco = await client.GetDiscoveryDocumentAsync(IdentityServerEndpoint);
                if (disco.IsError)
                {
                    Console.WriteLine(disco.Error);
                    return null;
                }


                var tokenResponse = await client.RequestClientCredentialsTokenAsync(new ClientCredentialsTokenRequest
                {
                    Address = disco.TokenEndpoint,
                    ClientId = "m2m.client",
                    ClientSecret = "511536EF-F270-4058-80CA-1C89C192F69A",
                    Scope = "scope1"
                });

                if (tokenResponse.IsError)
                {
                    Console.WriteLine(tokenResponse.Error);
                    return null;
                }

             
                return tokenResponse;
            }

        }


        static async Task<string> MakeTokenClaimsRequest(string token)
        {
            using (var client = new HttpClient())
            {
                client.SetBearerToken(token);

                var response = await client.GetAsync($"{IkeaApiEndpoint}/token");
                if (!response.IsSuccessStatusCode)
                {
                    Console.WriteLine(response.StatusCode);
                    return null;
                }
              
               return await response.Content.ReadAsStringAsync();
            }

        }



        static async Task<string> MakeNamesRequest(string token)
        {
            using (var client = new HttpClient())
            {
                client.SetBearerToken(token);

                var response = await client.GetAsync($"{IkeaApiEndpoint}/ikeanames");
                if (!response.IsSuccessStatusCode)
                {
                    Console.WriteLine(response.StatusCode);
                    return null;
                }

                return await response.Content.ReadAsStringAsync();
            }

        }


      
    }
}